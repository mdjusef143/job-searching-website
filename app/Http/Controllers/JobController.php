<?php

namespace App\Http\Controllers;

use App\Models\job;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function addJobPost(){
        return view('admin.job.add-job-post');
    }
    public function saveJobPost(Request $request){
        $jobs = new job();
        $jobs->category_id = $request->category_id;
        $jobs->company_id = $request->company_id;
        $jobs->vacancy = $request->vacancy;
        $jobs->employment_status = $request->employment_status;
        $jobs->experience = $request->experience;
        $jobs->age = $request->age;
        $jobs->job_location = $request->job_location;
        $jobs->salary = $request->salary;
        $jobs->job_responsibilities = $request->job_responsibilities;
        $jobs->educational = $request->educational;
        $jobs->skills = $request->skills;
        $jobs->compensation = $request->compensation;
        $jobs->job_source = $request->job_source;
        $jobs->published_date = $request->published_date;
        $jobs->Application_deadline_date = $request->Application_deadline_date;
        $jobs->save();
        return back();
    }
    public function manageJobPost(){
        return view('admin.job.manage-job-post',[
            'jobs'=>job::all()
        ]);
    }
}
